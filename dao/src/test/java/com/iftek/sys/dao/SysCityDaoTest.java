package com.iftek.sys.dao;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.junit5.api.DBRider;
import com.iftek.atom.testcomponent.PostgresDaoTestBase;
import com.iftek.sys.constant.AtomKitConst;
import com.iftek.sys.vo.SysCityVo;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("postgres")
@ActiveProfiles(value = AtomKitConst.Profile.LIQUIBASE)
@DBRider
class SysCityDaoTest extends PostgresDaoTestBase {

  @Autowired
  private SysCityDao sysCityDao;

  @Test
  void insert() throws Exception {
    SysCityVo sysCityVo = new SysCityVo();
    sysCityVo.setCityName("rock");
    int insert = sysCityDao.insert(sysCityVo);
    //haha
    assertThat(insert).isEqualTo(1);
  }

  @Test
  @DataSet("dao/SysCityDao/query.yml")
  @ExpectedDataSet("dao/SysCityDao/queryExpected.yml")
  void query() throws Exception {
    SysCityVo one = sysCityDao.getByPk(1L);
    one.setCityName(one.getCityName().replace("x", "y"));
    sysCityDao.update(one);

    SysCityVo two = sysCityDao.getByPk(2L);
    two.setCityName(two.getCityName().replace("x", "y"));
    sysCityDao.update(two);
  }
}