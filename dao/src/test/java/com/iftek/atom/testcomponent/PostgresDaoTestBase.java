package com.iftek.atom.testcomponent;

import com.iftek.atom.config.CommonPersistenceConfig;
import com.iftek.atom.config.DataSourceConfig;
import com.iftek.sys.constant.AtomKitConst;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * <code>@Transactional</code>      這個宣告會套用到 class 底下的所有 methods 做 Transactional 管理, 當測試單元 <code>@Test</code> 執行結束後, Database 的操作皆會做 Rollback.
 * <code>@ActiveProfiles</code>     這個宣告用來表示要啟用的對應的 <code>@Profile</code> 設定.
 * <code>@SpringJUnitConfig</code>  這個宣告包含 <code>@ExtendWith(SpringExtension.class)</code> 與 <code>@ContextConfiguration</code> 這兩種功能
 * 細節可參閱 <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html#integration-testing-annotations-junit-jupiter-springjunitconfig">Docs </a>.
 */
@ActiveProfiles(value = AtomKitConst.Profile.DEV_TEST)
@SpringJUnitConfig(value = {DataSourceConfig.class, CommonPersistenceConfig.class})
public class PostgresDaoTestBase {

  protected static final Logger log = LoggerFactory.getLogger(PostgresDaoTestBase.class);

  protected static final String COM_ID_53743179_STR = "53743179";

  @BeforeAll
  protected static void beforeAll() throws Exception {
    log.info("Before all.");
  }

  @AfterAll
  protected static void afterAll() throws Exception {
    log.info("After all.");
  }

  @BeforeEach
  protected void beforeEach() throws Exception {
    log.info("Before each.");
  }

  @AfterEach
  protected void afterEach() throws Exception {
    log.info("After each.");
  }

  /**
   * 驗證 BigDecimal 是否相同
   *
   * @param expect
   * @param actual
   */
  protected void assertBigDecimalEquals(final String expect, final BigDecimal actual) {
    assertThat(new BigDecimal(expect), Matchers.comparesEqualTo(actual));
  }

  /**
   * 驗證 collection 大小為 0
   *
   * @param collection
   */
  protected void assertSizeIsZero(final Collection collection) {
    assertEquals(0, collection.size());
  }

  /**
   * 驗證 collection 大小為 1
   *
   * @param collection
   */
  protected void assertSizeIsOne(final Collection collection) {
    assertEquals(1, collection.size());
  }

  /**
   * 驗證 collection 不為空
   *
   * @param collection
   */
  protected void assertCollectionIsNotEmpty(final Collection collection) {
    assertNotNull(collection);
    assertFalse(collection.isEmpty());
  }

  /**
   * 驗證 新增成功
   *
   * @param inserted
   */
  protected void assertInsertSuccess(final int inserted) {
    assertThat(inserted, greaterThan(0));
  }

  /**
   * 驗證 更新成功, 更新時 Mybatis 回傳更新成功的資料筆數,
   * 故驗證時, 最少要有 1 筆以上的更新成功
   *
   * @param updated
   */
  protected void assertUpdateSuccess(final int updated) {
    assertThat(updated, greaterThan(0));
  }

  /**
   * 驗證 更新失敗
   *
   * @param updated
   */
  protected void assertUpdateFailed(int updated) {
    assertEquals(0, updated);
  }

  /**
   * 驗證 刪除成功
   *
   * @param deleted
   */
  protected void assertDeleteSuccess(int deleted) {
    assertThat(deleted, greaterThan(0));
  }

  /**
   * 驗證只新增 1 筆
   *
   * @param inserted
   */
  protected void assertInsertOnce(final int inserted) {
    assertIsOne(inserted);
  }

  /**
   * 驗證只更新 1 筆
   *
   * @param updated
   */
  protected void assertUpdateOnce(final int updated) {
    assertIsOne(updated);
  }

  /**
   * 驗證只刪除 1 筆
   *
   * @param deleted
   */
  protected void assertDeleteOnce(final int deleted) {
    assertIsOne(deleted);
  }

  /**
   * 驗證 str 是否為 json
   *
   * @param str
   */
  protected void assertIsJson(final String str) {
    assertTrue(isJsonObject(str) || isJsonArray(str));
  }

  /**
   * 驗證 PK 欄位, 會大於 1
   *
   * @param pkColumn
   */
  protected void assertPkColumnMustBeGreaterThanOne(final Long pkColumn) {
    assertThat(pkColumn, greaterThan(1L));
  }

  /**
   * 驗正 HashMap 一致,
   * 檢查所有的 Key, Value 皆與預期的 HashMap 相同
   *
   * @param expected
   * @param actual
   */
  protected void assertHashMapEquals(final Map<String, Object> expected, final Map<String, Object> actual) {
    assertEquals(expected.size(), actual.size());
    boolean areEquals = expected.entrySet().stream()
        .allMatch(exp -> exp.getValue().equals(actual.get(exp.getKey())));
    assertTrue(areEquals);
  }

  /**
   * 驗證 HashMap 的 Key 為大寫
   *
   * @param verified
   */
  protected void assertKeysAreSnakeUpperCase(final Map<String, Object> verified) {
    verified.forEach((key, value) -> assertEquals(key.toUpperCase(), key));
  }

  /**
   * 驗證 D19 日期格式
   *
   * @param datetime
   */
  protected void assertD19Format(final String datetime) {
    final String d19Format = "yyyy-MM-dd HH:mm:ss";
    assertTrue(verifiedDateTimeFormat(datetime, d19Format));
  }

  /**
   * 驗證 D16 日期格式
   *
   * @param datetime
   */
  protected void assertD16Format(final String datetime) {
    final String d16Format = "yyyy-MM-dd HH:mm";
    assertTrue(verifiedDateTimeFormat(datetime, d16Format));
  }

  /**
   * 驗證 D10 日期格式
   *
   * @param datetime
   */
  protected void assertD10Format(final String datetime) {
    final String d10Format = "yyyy-MM-dd";
    assertTrue(verifiedDateTimeFormat(datetime, d10Format));
  }

  private boolean verifiedDateTimeFormat(final String datetime, final String dateTimePattern) {
    final String d19Format = "yyyy-MM-dd HH:mm:ss";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimePattern);
    try {
      LocalDateTime.parse(datetime, formatter);
      return true;

    } catch (DateTimeParseException formatExp) {
      return false;
    }
  }

  private void assertIsOne(final int verified) {
    assertEquals(1, verified);
  }

  private boolean isJsonObject(final String str) {
    try {
      new JSONObject(str);
      return true;
    } catch (JSONException e) {
      return false;
    }
  }

  private boolean isJsonArray(final String str) {
    try {
      new JSONArray(str);
      return true;
    } catch (JSONException e) {
      return false;
    }
  }
}
