package com.iftek.atom.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.InstantTypeHandler;
import org.apache.ibatis.type.JapaneseDateTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.LocalDateTimeTypeHandler;
import org.apache.ibatis.type.LocalDateTypeHandler;
import org.apache.ibatis.type.LocalTimeTypeHandler;
import org.apache.ibatis.type.MonthTypeHandler;
import org.apache.ibatis.type.OffsetDateTimeTypeHandler;
import org.apache.ibatis.type.OffsetTimeTypeHandler;
import org.apache.ibatis.type.TypeHandler;
import org.apache.ibatis.type.YearMonthTypeHandler;
import org.apache.ibatis.type.YearTypeHandler;
import org.apache.ibatis.type.ZonedDateTimeTypeHandler;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@PropertySource({"classpath:atom.properties"})
public class CommonPersistenceConfig {

  private static final String MAPPER_LOCATIONS = "classpath*:myBatis/oracle/**/*.xml";
  private static final String BASE_PACKAGE = "com.iftek.sys.dao";
  private static final String TYPE_ALIASES_PACKAGE = "com.iftek.sys.vo.base;com.iftek.sys.dao.base;com.iftek.sys.vo;com.iftek.sys.dao";

  @Bean
  public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
    SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
    sqlSessionFactoryBean.setDataSource(dataSource);
    sqlSessionFactoryBean.setTypeAliasesPackage(TYPE_ALIASES_PACKAGE);
//    sqlSessionFactoryBean.setPlugins(new Interceptor[]{new DynamicSqlHandler()});
    sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(MAPPER_LOCATIONS));
    sqlSessionFactoryBean.setTypeHandlers(getTypeHandlers());
    SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBean.getObject();
    org.apache.ibatis.session.Configuration configuration = sqlSessionFactory.getConfiguration();
    configuration.setMapUnderscoreToCamelCase(true);
    configuration.setJdbcTypeForNull(JdbcType.NULL);
    // do
    return sqlSessionFactory;
  }

  private TypeHandler[] getTypeHandlers() {
    List<TypeHandler> list = new ArrayList<>();
    list.add(new InstantTypeHandler());
    list.add(new JapaneseDateTypeHandler());
    list.add(new LocalDateTimeTypeHandler());
    list.add(new LocalDateTypeHandler());
    list.add(new LocalTimeTypeHandler());
    list.add(new MonthTypeHandler());
    list.add(new OffsetDateTimeTypeHandler());
    list.add(new OffsetTimeTypeHandler());
    list.add(new YearMonthTypeHandler());
    list.add(new YearTypeHandler());
    list.add(new ZonedDateTimeTypeHandler());
    return list.toArray(new TypeHandler[0]);
  }

  @Bean
  public MapperScannerConfigurer mapperScannerConfigurer() {
    MapperScannerConfigurer msc = new MapperScannerConfigurer();
    msc.setBasePackage(BASE_PACKAGE);
    msc.setSqlSessionFactoryBeanName("sqlSessionFactory");
    return msc;
  }

  @Bean
  @Primary
  public DataSourceTransactionManager transactionManager(DataSource dataSource) {
    DataSourceTransactionManager tm = new DataSourceTransactionManager();
    tm.setDataSource(dataSource);
    return tm;
  }
}
