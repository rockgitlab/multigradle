package com.iftek.atom.config;

import com.iftek.sys.constant.AtomKitConst;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource({"classpath:atom.properties"})
public class DataSourceConfig implements EnvironmentAware {
  private static final String PROPERTY_SYSTEM_CONFIG_DB_ORACLE_JNDI = "system.config.db.oracle.jndi";
  private static final String PROPERTY_SYSTEM_CONFIG_DB_POSTGRES_JNDI = "system.config.db.postgres.jndi";

  private Environment env;

  @Profile({"!dev"})
  @Bean(name = "dataSource")
  @Primary
  public DataSource dataSource() {
    JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
    dsLookup.setResourceRef(true);
    return dsLookup.getDataSource(env.getRequiredProperty(PROPERTY_SYSTEM_CONFIG_DB_POSTGRES_JNDI));
  }

//  @Profile({AtomKitConst.Profile.DEV_TEST, AtomKitConst.Profile.UNIT_TEST})
//  @Bean(name = "dataSource")
//  public DataSource dataSourceDev() {
//    // todo Jerry [階段性] Postgres DAO 測試用, 整合完畢後移除
//    DriverManagerDataSource dataSource = new DriverManagerDataSource();
//    dataSource.setDriverClassName("org.postgresql.Driver");
//    dataSource.setUrl("jdbc:postgresql://localhost:5432/wms");
//    dataSource.setUsername("wms");
//
//    dataSource.setPassword("948794dj;6");
//    dataSource.setConnectionProperties(this.getProperties(dataSource));
//    return new TransactionAwareDataSourceProxy(dataSource);
//  }

  @Profile({AtomKitConst.Profile.DEV_TEST, AtomKitConst.Profile.UNIT_TEST})
  @Bean(name = "dataSource")
  public DataSource dataSourceDev() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("org.h2.Driver");
    dataSource.setUrl("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1");
    dataSource.setUsername("sa");
    dataSource.setPassword("sa");

    return dataSource;
  }

  @Profile({AtomKitConst.Profile.LIQUIBASE})
  @Bean
  public SpringLiquibase liquibase(DataSource dataSource) {
    SpringLiquibase liquibase = new SpringLiquibase();
    liquibase.setDataSource(dataSource);
    liquibase.setChangeLog("classpath:config/liquibase/master.xml");
    return liquibase;
  }

  @Override
  public void setEnvironment(Environment environment) {
    this.env = environment;
  }

  /**
   * The properties setting follow Wiildfly9 Datasource setting,
   *
   * @param dataSource
   * @return
   */
  private Properties getProperties(final DriverManagerDataSource dataSource) {
    Properties properties = dataSource.getConnectionProperties();
    if (properties == null) {
      properties = new Properties();
    }
    // The property will sent String value to Postgres server will attempt to infer an appropriate type
    properties.setProperty("stringtype", "unspecified");
    return properties;
  }
}
