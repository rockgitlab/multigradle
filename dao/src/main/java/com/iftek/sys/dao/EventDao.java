package com.iftek.sys.dao;

import com.iftek.atom.entity.Event;

public interface EventDao {
  Event query(String eventName);
}
