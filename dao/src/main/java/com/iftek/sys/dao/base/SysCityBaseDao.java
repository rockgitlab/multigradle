package com.iftek.sys.dao.base;

import com.iftek.sys.vo.SysCityVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Do not modify this file!
 * For extending functions, edit the SysCityDao file please.
 * Genertion Time : 2018-11-23 17:10:43
 */
public interface SysCityBaseDao {

    List<SysCityVo> query(Object params) throws Exception;

    int insert(Object params) throws Exception;

    List<SysCityVo> listAll() throws Exception;

    Integer count(Object params) throws Exception;

    int delete(Object params) throws Exception;

    SysCityVo getByPk(@Param("cityNum") Long cityNum) throws Exception;

    int deleteByPk(@Param("cityNum") Long cityNum) throws Exception;

    int update(Object params) throws Exception;

    int forceUpdate(Object params) throws Exception;

}
