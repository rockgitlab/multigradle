package com.iftek.sys.constant;

public class AtomKitConst {
  public class Profile {
    public static final String UNIT_TEST = "unittest";
    public static final String DEV_TEST = "dev";
    public static final String LIQUIBASE = "liquibase";
  }
}
