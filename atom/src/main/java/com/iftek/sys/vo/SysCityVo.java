package com.iftek.sys.vo;

import com.iftek.sys.vo.base.SysCityBaseVo;
import lombok.Getter;
import lombok.Setter;

/**
 * For extending functions, edit this file please.
 */
@Getter
@Setter
public class SysCityVo extends SysCityBaseVo {

  public SysCityVo() {
    super();
  }

}
