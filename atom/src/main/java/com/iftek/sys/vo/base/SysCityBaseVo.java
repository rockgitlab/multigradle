package com.iftek.sys.vo.base;

import javax.persistence.Transient;

/**
 * Do not modify this file!
 * For extending functions, edit the SysCityVo file please.
 * Genertion Time : 2018-11-23 17:10:43
 */
public class SysCityBaseVo extends BaseVo {

  public SysCityBaseVo() {
  }

  private Long cityNum;

  private String cityName;

  public Long getCityNum() {
    return this.cityNum;
  }

  public void setCityNum(Long cityNum) {
    this.cityNum = cityNum;
  }

  public String getCityName() {
    return this.cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  /**
   * Transient
   */
  @Transient
  private String comId;

  /**
   * Transient
   *
   * @return
   */
  public String getComId() {
    return this.comId;
  }

  /**
   * Transient
   *
   * @param comId
   */
  public void setComId(String comId) {
    this.comId = comId;
  }

}
